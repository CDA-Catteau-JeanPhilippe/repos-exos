package com.afpa.excercice2;

public class TstPnt {
	public static void main(String args[]) {
//		Point a ;
//		a = new Point(3, 5) ;
//		a.affiche() ;
//		a.deplace(2, 0) ;
//		a.affiche() ;
//		Point b = new Point(6, 8) ;
//		b.affiche() ;
		
		Point a;
		Point b;
		a = new Point(3, 5);
		System.out.println("Je suis un point de coordonnees " + a.abscisse() + " " + a.ordonnee());
		a.deplace(2, 0);
		System.out.println("Je suis un point de coordonnees " + a.abscisse() + " " + a.ordonnee());
		b = new Point(6, 8);
		System.out.println("Je suis un point de coordonnees " + b.abscisse() + " " + b.ordonnee());
	}
}