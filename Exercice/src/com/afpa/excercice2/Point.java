package com.afpa.excercice2;

class Point {
	public Point(int abs, int ord) {
		x = abs;
		y = ord;
	}

	public void deplace(int dx, int dy) {
		x += dx;
		y += dy;
	}

	public double abscisse() {
		double a = this.x;
		return a;
	}

	public double ordonnee() {
		double b = this.y;
		return b;
	}

	private double x; // abscisse
	private double y; // ordonnee
}