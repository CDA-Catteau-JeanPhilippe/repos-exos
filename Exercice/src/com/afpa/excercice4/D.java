package com.afpa.excercice4;

import java.text.SimpleDateFormat;
import java.util.Date;

public class D {	
	public static void main (String[] args) {
		// Cr�er un premier objet java.util.Date
		Date d = new Date();
		// Afficher cette objet avec un syso
		System.out.println("d"+d);
		System.out.println("d, milliseconde :"+d.getTime());
		
		// Cr�er un second objet java.util.Date
		Date d2 = new Date();
		// Afficher cette objet avec un syso
		System.out.println(d2);
		System.out.println("d, milliseconde :"+d2.getTime());
		
		boolean compare = d.after(d2);
		System.out.println(compare);
		
		compare = d2.after(d);
		System.out.println(compare);
			
		String patternYMD = "yyyy/MM/dd";
		SimpleDateFormat sdf = new SimpleDateFormat(patternYMD);

		String s = sdf.format(d); // format permet de mettre en parametre une date
		System.out.println(s);
		
		String patternHM = "HH:mm:ss";
		sdf = new SimpleDateFormat(patternHM);

		String s1 = sdf.format(d); // format pertmet de mettre en parametre une date
		System.out.println(s1);		
	}
}
