package com.afpa.excercice0;

public class Article {
	//////////////////////////////////////////////////////
	//------------------- ATTRIBUTS -------------------//
	/////////////////////////////////////////////////////
	private int reference = 0;
	private String designation = "";
	private static int prixHT = 0;
	private static int tauxTVA = 20;
	//////////////////////////////////////////////////////
	//--------------- GETTERS ET SETTERS ---------------//
	/////////////////////////////////////////////////////
	public void setReference(int uneReference) {
		reference = uneReference;
	} 

	public int getReference() {
		return reference;
	}

	public void setDesignation(String uneDesignation) {
		designation = uneDesignation;
	}

	public String getDesignation() {
		return designation;
	}

	public void setPrixHT(int unPrixHT) {
		prixHT = unPrixHT;
	}

	public int getPrixHT() {
		return prixHT;
	}

	public void setTauxTVA(int unTauxTVA) {
		tauxTVA = unTauxTVA;
	}

	public int getTauxTVA() {
		return tauxTVA;
	}
	///////////////////////////////////////////////////////
	//------------------ CONSTRUCTEURS ------------------//
	//////////////////////////////////////////////////////	
	public Article() {
		reference = 0;
		designation = "";
		prixHT = 0;
	}

	public Article(int uneReference, String uneDesignation) {
		reference = uneReference;
		designation = uneDesignation;
	}

	public Article(int uneReference, String uneDesignation, int unPrixHT) {
		reference = uneReference;
		designation = uneDesignation;
		prixHT = 0;
	}
	///////////////////////////////////////////////////////
	//--------------------- METHODES --------------------//
	//////////////////////////////////////////////////////
	public static double CalculerPrixTTC(Article article) {
		double res = prixHT + (prixHT * tauxTVA / 100);
		return res;
	}

	public static void AfficherArticle() {
		System.out.println("Donner le taux de TVA pour tous les articles : " + Article.tauxTVA);
		System.out.println("Le taux TVA est : " + Article.tauxTVA + "%\n\n");

		Article article1 = new Article();

		System.out.println("Article 1 :\n"
				+ "Référence : " + article1.reference 
				+ "\nDésignation : " + article1.designation
				+ "\nPrix HT : " + Article.prixHT 
				+ "\nLe prix TTC est de " + (int)CalculerPrixTTC(article1)
				+ "\n\n");

		Article article2 = new Article(111, "ATA", 100);

		System.out.println("Article 2 :\n"
				+ "Donner la référence : " + article2.reference 
				+ "\nDonner la désignation : " + article2.designation 
				+ "\nDonner le prix HT : " + Article.prixHT 
				+ "\nRéférence : " + article2.reference 
				+ "\nDésignation : " + article2.designation 
				+ "\nPrix HT : " + Article.prixHT 
				+ "\nLe prix TTC est de " + (int)CalculerPrixTTC(article2) 
				+ "\n\n");

		Article article3 = new Article(122, "RER");

		System.out.println("Article 3 :" 
				+ "\nDonner la référence : " + article3.reference 
				+ "\nDonner la désignation : " + article3.designation 
				+ "\nRéférence : " + article3.reference 
				+ "\nDésignation : " + article3.designation 
				+ "\nPrix HT : " + Article.prixHT 
				+ "\nLe prix TTC est de " + (int)CalculerPrixTTC(article3) 
				+ "\n\n");
	}
}
