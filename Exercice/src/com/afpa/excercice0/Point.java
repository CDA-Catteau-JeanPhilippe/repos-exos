package com.afpa.excercice0;

public class Point {
	
///////////////////////////////////////////////////////
// ------------------- ATTRIBUTS ------------------- //
//////////////////////////////////////////////////////
	private double abscisse;
	private double ordonne;
////////////////////////////////////////////////////////
//----------------- CONSTRUCTEURS ----------------- //
///////////////////////////////////////////////////////	
	public Point () {

	}

	public Point (double unAbscisse, double unOrdonne) {
		abscisse = unAbscisse;
		ordonne = unOrdonne;
	}
////////////////////////////////////////////////////////
// -------------------- METHODES -------------------- //
///////////////////////////////////////////////////////
	public static void Norme() {
		Point point = new Point();
		point.setX(1);
		point.setY(2);
		double x = point.getX();
		double y = point.getY();
		double res = Math.sqrt(x*x+y*y);
		System.out.println("Donner l'abscisse : "+x);
		System.out.println("Donner l'ordonne : "+y);
		System.out.println("La norme du point ("+(int)x+","+(int)y+") est: "+res);
	}
//////////////////////////////////////////////////////
// -------------- GETTERS ET SETTERS -------------- //
/////////////////////////////////////////////////////
	public void setX(double unAbscisse) {
		abscisse = unAbscisse;
	}
	public double getX() {
		return abscisse;
	}

	public void setY(double unOrdonne) {
		ordonne = unOrdonne;
	}
	public double getY() {
		return ordonne;
	}
}