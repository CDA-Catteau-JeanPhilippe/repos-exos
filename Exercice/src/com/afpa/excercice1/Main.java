package com.afpa.excercice1;

public class Main {
	public static void main(String[] args) {
		PointAxe b = new PointAxe('b', 11);
		PointAxe c = new PointAxe('c', 21);

		b.affiche();
		c.affiche();
		c = b;
		b.translate(5);
		System.out.println("apres translate : ");
		b.affiche();
		c.affiche();
	}
}
