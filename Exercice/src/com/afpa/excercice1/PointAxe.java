package com.afpa.excercice1;

public class PointAxe {
//-------------------------------------------------//
//------------------- ATTRIBUTS -------------------//
//-------------------------------------------------//

	char nom;
	double abscisse;

//-------------------------------------------------//
//----------------- CONSTRUCTEURS -----------------//
//-------------------------------------------------//

	public PointAxe(char n, double a) {
		this.abscisse=a;
		this.nom=n;
	}
	
//-------------------------------------------------//
//------------------- METHODES --------------------//
//-------------------------------------------------//
	public void affiche() {
		System.out.println("nom : "+this.nom+", abscisse : "+this.abscisse);
	}
	
	public void translate(double paramAbscisse) {
		this.abscisse = this.abscisse+paramAbscisse;
	}
	
//-------------------------------------------------//
//-------------- GETTERS-ET-SETTERS ---------------//
//-------------------------------------------------//
//	
//	public static char getNom() {
//		return nom;
//	}
//
//	public void setNom(char unNom) {
//		nom = unNom;
//	}
//	
//	public double getAbscisse() {
//		return abscisse;
//	}
//	
//	public void setAbscisse(double unAbscisse) {
//		abscisse = unAbscisse;
//	}
}
