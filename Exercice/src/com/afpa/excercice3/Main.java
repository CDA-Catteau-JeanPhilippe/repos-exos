package com.afpa.excercice3;

public class Main {
	public static void main(String[] args) {
		NumeroUnique numUni1 = new NumeroUnique();
		numUni1.afficher();
		numUni1.afficher();
		
		NumeroUnique numUni2 = new NumeroUnique();
		numUni2.afficher();
		numUni1.afficher();
		
		NumeroUnique.initialiserCompteur();
		
		NumeroUnique numUni3 = new NumeroUnique();
		numUni1.afficher();
		numUni2.afficher();
		numUni3.afficher();
		
		NumeroUnique numUni4 = new NumeroUnique();
		numUni1.afficher();
		numUni2.afficher();
		numUni3.afficher();
		numUni4.afficher();
		
		NumeroUnique numUni5 = new NumeroUnique();
		numUni1.afficher();
		numUni2.afficher();
		numUni3.afficher();
		numUni4.afficher();
		numUni5.afficher();
	}
}
